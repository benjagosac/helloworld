using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScalingScript : MonoBehaviour
{

    [SerializeField]
    private Vector3 axes;
    public float scaleUnits;

    void Update()
    {
        axes = CapsuleMovementScript.ClampVector3(axes);
        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}
